from math import sqrt, tan, fabs
import matplotlib.pyplot as plt
import graphics
import computation
import numerically_compute

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    lmbd = 1.064 # mkm
    W = 2.0 * 1.18 # mkm
    # W = 2.0 * 1.93 - wrap with R = 2
    n1 = 1.57
    n3 = 1.55
    d = W / 2
    pi = 3.14151828
    R = W
    # R = W
    eps = 0.001
    points_chi_d = 1000
    c = 3 * 10 ** 2 # mkm/c
    mu = 1.26 * 10 ** (-1)
    # print(f'beta_0_adv = {computation.get_even_mode_beta_adv(points_chi_d, eps, pi / 2 - eps, lmbd, W, n1, n3)}')
    # print(f'beta_0_old = {computation.get_even_mode_beta(points_chi_d, eps, pi / 2 - eps, lmbd, W, n1, n3)}')
    # print(f'beta_1_adv = {computation.get_odd_mode_beta_adv(points_chi_d, pi / 2 + eps, 3 * pi / 2 - eps, lmbd, W, n1, n3)}')
    # print(f'beta_1_old = {computation.get_odd_mode_beta(points_chi_d, pi / 2 + eps, 3 * pi / 2 - eps, lmbd, W, n1, n3)}')
    L, c_0_new, c_1_new, chi_d_0, chi_d_1, c_0, c_1, new_R, gamma_0, gamma_1, beta_0, beta_1 =\
        computation.get_L_and_coeffs_by_sys_params(lmbd, W, n1, n3, R, points_chi_d)
    print(f'c_0_new = {c_0_new} 1/mkm')
    print(f'c_1_new = {c_1_new} 1/mkm')
    print(f'gamma_0 = {gamma_0} 1/mkm')
    print(f'gamma_1 = {gamma_1} 1/mkm')
    print(f'chi_d_0 = {chi_d_0}')
    print(f'chi_d_1 = {chi_d_1}')
    print(f'c_0 = {c_0}')
    print(f'c_1 = {c_1}')
    print(f'beta_0 = {beta_0} 1/mkm')
    print(f'beta_1 = {beta_1} 1/mkm')
    print(f'new_R = {new_R} mkm')
    print(f'L = {L} mkm')
    computation.print_system_transformation(c_0_new, c_1_new, L)
    eps = 0.001
    inpA = (0.5, 0.86602, 0.7071, 0.0)  # A00 A01 A10 A11=0
    # inpA = (0.0, 1.0, 0.7071, 0.0) # A00 A01 A10 A11=0
    # L = 2450
    Af_list0 = numerically_compute.propagate_approx1_fd(new_R, L, (chi_d_0 / d, chi_d_1 / d), (gamma_0, gamma_1),
                                                               (beta_0, beta_1), lmbd, W, n1, n3, inpA, c, mu)
    Af_list2 = numerically_compute.propagate_approx1_fd(new_R, L, (chi_d_0 / d, chi_d_1 / d), (gamma_0, gamma_1),
                                                        (beta_0, beta_1), lmbd, W, n1, n3, Af_list0, c, mu)
    # print(f'00: {abs(Af_list0[0])}\n01: {abs(Af_list0[1])}\n10: {abs(Af_list0[2])}\n11: {abs(Af_list0[3])}\n')
    print(f'00: {Af_list0[0]}\n01: {Af_list0[1]}\n10: {Af_list0[2]}\n11: {Af_list0[3]}\n')
    Af_list1 = numerically_compute.propagate_approx1_fd_explicit(new_R, L, (chi_d_0 / d, chi_d_1 / d), (gamma_0, gamma_1),
                                                        (beta_0, beta_1), lmbd, W, n1, n3, inpA, c, mu)
    # print(f'00: {abs(Af_list1[0])}\n01: {abs(Af_list1[1])}\n10: {abs(Af_list1[2])}\n11: {abs(Af_list1[3])}\n')
    print(f'00: {Af_list1[0]}\n01: {Af_list1[1]}\n10: {Af_list1[2]}\n11: {Af_list1[3]}\n')
    print(f'00: {Af_list2[0]}\n01: {Af_list2[1]}\n10: {Af_list2[2]}\n11: {Af_list2[3]}\n')
    print(f'intermodal error in final: {numerically_compute.compute_error(Af_list2, inpA)}')
    print(f'intermodal error before arm: {numerically_compute.compute_error(Af_list1, Af_list0)}')
    # graphics.plot_even(points_chi_d, eps, pi / 2 - eps, lmbd, W, n1, n3)
    # graphics.plot_even_adv(points_chi_d, eps, pi / 2 - eps, lmbd, W, n1, n3)
    # graphics.plot_odd_adv(points_chi_d, pi / 2 + eps, 3 * pi / 2 - eps, lmbd, W, n1, n3)
    # graphics.plot_of_d(250, lmbd, n1, n3)
    n_points_d = 500
    # graphics.plot_of_d(n_points_d, lmbd, n1, n3, R)
    # graphics.plot_c_ratio_of_d(n_points_d, lmbd, n1, n3, R)
