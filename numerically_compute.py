from math import sqrt, tan, pi, sin, cos, sinh, cosh, exp, ceil, fabs, log, e, floor, atan
import computation
import cmath


def propagate_approx1_analytic(R, L, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, A_list):
    A00, A01, A10, A11 = A_list
    chi0, chi1 = chi_pair
    gamma0, gamma1 = gamma_pair
    beta0, beta1 = beta_pair
    c0 = computation.get_coupling_coeff_even(R, chi0, gamma0, beta0, lmbd, W, n1, n3)
    c1 = computation.get_coupling_coeff_odd(R, chi1, gamma1, beta1, lmbd, W, n1, n3)
    A00f = A00 * cmath.cos(c0 * L) + complex(0, 1) * A10 * cmath.sin(c0 * L)
    A10f = A10 * cmath.cos(c0 * L) + complex(0, 1) * A00 * cmath.sin(c0 * L)
    A01f = A01 * cmath.cos(c1 * L) + complex(0, 1) * A11 * cmath.sin(c1 * L)
    A11f = A11 * cmath.cos(c1 * L) + complex(0, 1) * A01 * cmath.sin(c1 * L)
    return A00f, A01f, A10f, A11f


def compute_c(ind, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu):
    d = W / 2
    chi_0, chi_1 = chi_pair
    gamma_0, gamma_1 = gamma_pair
    beta_0, beta_1 = beta_pair
    if ind == 0:
        return -n1 * beta_0 * lmbd * (2 * d * gamma_0 * chi_0 + 2 * chi_0 * (cos(chi_0 * d)) ** 2
                 + gamma_0 * sin(2 * chi_0 * d)) / (2 * pi * c * mu * gamma_0 * chi_0)
    else:
        return -(n1 * beta_1 * lmbd * (2 * d * gamma_1 * chi_1 + 2 * chi_1 * (sin(chi_1 * d)) ** 2
                 - gamma_1 * sin(2 * chi_1 * d))) / (2 * pi * c * mu * gamma_1 * chi_1)


def compute_D(m1, m2, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu):
    d = W / 2
    chi_0, chi_1 = chi_pair
    gamma_0, gamma_1 = gamma_pair
    beta_0, beta_1 = beta_pair
    if m1 == 0 == m2:
        return -2 * exp(-gamma_0 * R) * (n1 ** 2 - n3 ** 2) * pi * cos(d * chi_0) * (-gamma_0 * cos(d * chi_0)
                                    + exp(2 * d * gamma_0) * gamma_0 * cos(d * chi_0) + chi_0 * sin(d * chi_0)
                                    + exp(2 * d * gamma_0) * chi_0 * sin(d * chi_0)) / (c * n1 * lmbd * mu * (gamma_0 ** 2 + chi_0 ** 2))
    if m1 == 1 and m2 == 0:
        return -2 * exp(-R * gamma_1) * (n1 ** 2 - n3 ** 2) * pi / (c * n1 * lmbd * mu * (gamma_1 ** 2 + chi_0 ** 2)) \
               * (-exp(d * gamma_1) * gamma_1 - exp((d - R) * gamma_1 + R * gamma_1) * gamma_1 + gamma_1 * cos(d * chi_0)
               + exp(d * gamma_1 + (d - R) * gamma_1 + R * gamma_1) * gamma_1 * cos(d * chi_0) - chi_0 * sin(d * chi_0)
               + exp(d * gamma_1 + (d - R) * gamma_1 + R * gamma_1) * chi_0 * sin(d * chi_0)) * sin(d * chi_1)
    if m1 == 0 and m2 == 1:
        return - 2 * exp(-R * gamma_0) * (n1 ** 2 - n3 ** 2) * pi * cos(d * chi_0) \
               * (chi_1 * cos(d * chi_1) - exp(2 * d * gamma_0) * chi_1 * cos(d * chi_1) + gamma_0 * sin(d * chi_1)
               + exp(2 * d * gamma_0) * gamma_0 * sin(d * chi_1)) /\
               (c * n1 * lmbd * mu * (gamma_0 ** 2 + chi_1 ** 2))
    if m1 == 1 and m2 == 1:
        return 2 * exp(-R * gamma_1) * (n1 ** 2 - n3 ** 2) * pi * sin(d * chi_1) / (c * n1 * lmbd * mu * (gamma_1 ** 2 + chi_1 ** 2)) \
               * (exp(d * gamma_1) * chi_1 + exp((d - R) * gamma_1 + R * gamma_1) * chi_1 - chi_1 * cos(d * chi_1)
               - exp(d * gamma_1 + (d - R) * gamma_1 + R * gamma_1) * chi_1 * cos(d * chi_1) - gamma_1 * sin(d * chi_1)
               + exp(d * gamma_1 + (d - R) * gamma_1 + R * gamma_1) * gamma_1 * sin(d * chi_1))

# def propagate_approx1_fd(R, L, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, A_list, c, mu):
#     n_steps = 10000
#     dz = L / n_steps
#     A00, A01, A10, A11 = A_list
#     chi0, chi1 = chi_pair
#     gamma0, gamma1 = gamma_pair
#     beta0, beta1 = beta_pair
#     A00im1, A01im1, A10im1, A11im1 = A_list
#     c0 = compute_c(0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
#     c1 = compute_c(1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
#     D00 = compute_D(0, 0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
#     D01 = compute_D(0, 1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
#     D10 = compute_D(1, 0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
#     D11 = compute_D(1, 1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
#     for i in range(n_steps):
#         if A00im1 == 0:
#             A00i = A00im1
#         else:
#             A00i = A00im1 - complex(0, 1) * (A10im1 ** 2 * D00) * dz / (A00im1 * c0)
#         if A01im1 == 0:
#             A01i = A01im1
#         else:
#             A01i = A01im1 - complex(0, 1) * (A11im1 ** 2 * D11) * dz / (A01im1 * c1)
#         if A10im1 == 0:
#             A10i = A10im1
#         else:
#             A10i = A10im1 - complex(0, 1) * (A00im1 ** 2 * D00) * dz / (A10im1 * c1)
#         if A11im1 == 0:
#             A11i = A11im1
#         else:
#             A11i = A11im1 - complex(0, 1) * (A01im1 ** 2 * D11) * dz / (A11im1 * c1)
#         A00im1 = A00i
#         A01im1 = A01i
#         A10im1 = A10i
#         A11im1 = A11i
#     A00f = A00i
#     A10f = A01i
#     A01f = A10i
#     A11f = A11i
#     return A00f, A01f, A10f, A11f

def propagate_approx1_fd_explicit(R, L, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, A_list, c, mu):
    n_steps = 100000
    dz = L / n_steps
    A00, A01, A10, A11 = A_list
    chi0, chi1 = chi_pair
    gamma0, gamma1 = gamma_pair
    beta0, beta1 = beta_pair
    A00im1, A01im1, A10im1, A11im1 = A_list
    I = complex(0, 1)
    dBeta = beta0 - beta1
    c0 = compute_c(0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    c1 = compute_c(1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D00 = compute_D(0, 0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D01 = compute_D(0, 1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D10 = compute_D(1, 0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D11 = compute_D(1, 1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    for i in range(n_steps):
        z = dz * i
        A00i = -dz * I * D00 * A10im1 / c0 + A00im1
        A01i = -dz * I * D11 * A11im1 / c1 + A01im1
        A10i = -dz * I * D00 * A00im1 / c0 + A10im1
        A11i = -dz * I * D11 * A01im1 / c1 + A11im1
        A00im1 = A00i
        A01im1 = A01i
        A10im1 = A10i
        A11im1 = A11i
    A00f = A00i
    A10f = A10i
    A01f = A01i
    A11f = A11i
    return A00f, A01f, A10f, A11f


def propagate_approx1_fd(R, L, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, A_list, c, mu):
    n_steps = 200000
    dz = L / n_steps
    A00, A01, A10, A11 = A_list
    chi0, chi1 = chi_pair
    gamma0, gamma1 = gamma_pair
    beta0, beta1 = beta_pair
    A00im1, A01im1, A10im1, A11im1 = A_list
    I = complex(0, 1)
    dBeta = beta0 - beta1
    c0 = compute_c(0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    c1 = compute_c(1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D00 = compute_D(0, 0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D01 = compute_D(0, 1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D10 = compute_D(1, 0, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    D11 = compute_D(1, 1, R, chi_pair, gamma_pair, beta_pair, lmbd, W, n1, n3, c, mu)
    print(f'c0: {c0}\nc1: {c1}\n')
    print(f'D00: {D00}\nD01: {D01}\nD10: {D10}\nD11: {D11}\n')
    for i in range(n_steps):
        z = dz * i
        A00i = -dz * I * D00 * A10im1 / c0 - dz * I * D10 * A11im1 * cmath.exp(I * dBeta * z) / c0 + A00im1
        A01i = -dz * I * D11 * A11im1 / c1 - dz * I * D01 * A10im1 * cmath.exp(-I * dBeta * z) / c1 + A01im1
        A10i = -dz * I * D00 * A00im1 / c0 - dz * I * D10 * A01im1 * cmath.exp(I * dBeta * z) / c0 + A10im1
        A11i = -dz * I * D11 * A01im1 / c1 - dz * I * D01 * A10im1 * cmath.exp(-I * dBeta * z) / c1 + A11im1
        A00im1 = A00i
        A01im1 = A01i
        A10im1 = A10i
        A11im1 = A11i
    A00f = A00i
    A10f = A10i
    A01f = A01i
    A11f = A11i
    return A00f, A01f, A10f, A11f


def print_system_state_in_L(Af_list):
    A00f, A01f, A10f, A11f = Af_list
    print(f'waveguide control: A0 = {A00f.real}, A1 = {A01f}')
    print(f'waveguide target: A0 = {A10f.real}, A1 = {A11f}')


def compute_error(Af_list, target_Af_list):
    """the power in target and real amplitudes lists are assumed to be equal"""
    Af_list_ampls = list(map((lambda x: abs(x) ** 2), Af_list))
    return (sum([(lambda x: x if x > 0 else 0)(abs(Af) ** 2 - abs(target_Af_list[Af_ind]) ** 2) for Af_ind, Af
                 in enumerate(Af_list)])) / sum(Af_list_ampls)
