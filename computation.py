from math import sqrt, tan, pi, sin, cos, sinh, cosh, exp, ceil, fabs, log, e, floor, atan


def logistic_func(x, ampl=10, scaler=1000, center=0.0):
    if x < 50:
        return ampl / (1 + e ** (-scaler * (fabs(x) - center)))
    else:
        return 0


def normaliser(x, mx=10):
    return mx * atan(1 / mx * x)


def get_func_length_approx(vals):
    res = 0
    for i, _ in enumerate(vals[:-1]):
        res += sqrt((vals[i + 1][0] - vals[i][0]) ** 2 + (vals[i + 1][1] - vals[i][1]) ** 2)
    return res


def get_even_mode_beta(points_chi_d, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    chi_d = [chi_d_min + i / points_chi_d * chi_d_max for i in range(0, points_chi_d)]
    gamma_over_chi = []
    for i in chi_d:
        try:
            gamma_over_chi.append(sqrt((n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2 / i ** 2 - 1))
        except ValueError:
            break
    gamma_over_chi_m_tan = [g_o_c - tan(chi_d[j]) for j, g_o_c in enumerate(gamma_over_chi)]
    l = 0
    r = len(gamma_over_chi) - 1
    if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] > 0:
        raise ValueError('There is no solution in this region')
    while gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] < 0 and r - l > 1:
        m = int(l + (r - l) / 2)
        if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[m] < 0:
            r = m
        elif gamma_over_chi_m_tan[r] * gamma_over_chi_m_tan[m] < 0:
            l = m
        else:
            break
    return chi_d[m]


def get_odd_mode_beta(points_chi_d, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    chi_d = [chi_d_min + i / points_chi_d * chi_d_max for i in range(0, points_chi_d)]
    gamma_over_chi = []
    for i in chi_d:
        try:
            gamma_over_chi.append(-sqrt(1 / ((n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2 / i ** 2 - 1)))
        except ValueError:
            break
    gamma_over_chi_m_tan = [g_o_c - tan(chi_d[j]) for j, g_o_c in enumerate(gamma_over_chi)]
    l = 0
    r = len(gamma_over_chi) - 1
    if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] > 0:
        raise ValueError('There is no solution in this region')
    while gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] < 0 and r - l > 1:
        m = int(l + (r - l) / 2)
        if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[m] < 0:
            r = m
        elif gamma_over_chi_m_tan[r] * gamma_over_chi_m_tan[m] < 0:
            l = m
        else:
            break
    return chi_d[m]


def get_even_mode_beta_adv(points_chi_d, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    max_num = 50
    func_cut_off = 3
    points_n_init = 500
    eps = 0.001
    a = (n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2
    func_gamma_o_chi = lambda chi_d: sqrt(a / chi_d ** 2 - 1)
    deriv_even = lambda a_inner, chi_d: a_inner / (sqrt(a_inner / chi_d ** 2 - 1) * chi_d ** 3) + 1 / cos(chi_d) ** 2
    chi_d_max_new = min(chi_d_max, sqrt(n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) * d) - eps
    chi_d_array_uni = [chi_d_min + i / points_n_init * (chi_d_max_new - chi_d_min) for i in range(0, points_n_init)]
    vals_uni = []
    for chi_d in chi_d_array_uni:
        if fabs(normaliser(tan(chi_d) - func_gamma_o_chi(chi_d), max_num)) > func_cut_off:
            continue
        vals_uni.append((chi_d, normaliser(tan(chi_d) - func_gamma_o_chi(chi_d), max_num)))
    chi_d_min_new = vals_uni[0][0]
    chi_d_max_new = vals_uni[-1][0]
    y_span = vals_uni[-1][1] - vals_uni[0][1]
    x_span = vals_uni[-1][0] - vals_uni[0][0]
    dy = y_span / points_chi_d
    dx_o_2 = x_span / (2 * points_chi_d)
    chi_d_array = [chi_d_min_new]
    gamma_over_chi = [normaliser((func_gamma_o_chi(chi_d_min_new)), max_num)]
    for i in range(points_chi_d):
        deriv = normaliser(deriv_even(a, chi_d_array[-1] + dx_o_2), max_num)
        dx = min(dy / deriv, x_span / points_chi_d)
        if chi_d_array[-1] + dx + dx_o_2 >= chi_d_max_new:
            break
        chi_d_array.append(chi_d_array[-1] + dx)
        gamma_over_chi.append(normaliser(func_gamma_o_chi(chi_d_array[-1]), max_num))
    gamma_over_chi_m_tan = [g_o_c - tan(chi_d_array[j]) for j, g_o_c in enumerate(gamma_over_chi)]
    l = 0
    r = len(gamma_over_chi) - 1
    if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] > 0:
        raise ValueError('There is no solution in this region')
    while gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] < 0 and r - l > 1:
        m = int(l + (r - l) / 2)
        if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[m] < 0:
            r = m
        elif gamma_over_chi_m_tan[r] * gamma_over_chi_m_tan[m] < 0:
            l = m
        else:
            break
    return chi_d_array[m]


def get_odd_mode_beta_adv(points_chi_d, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    eps = 0.00000001
    max_num = 500
    func_cut_off = 10
    points_n_init = max(500, points_chi_d)
    a = (n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2
    func_chi_o_gamma = lambda chi_d: -sqrt(1 / (a / chi_d ** 2 - 1))
    deriv_odd = lambda a_inner, chi_d: a_inner / (a_inner - chi_d ** 2) + 1 / cos(chi_d) ** 2
    chi_d_max_new = min(chi_d_max, sqrt(n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) * d) - eps
    chi_d_array_uni = [chi_d_min + i / points_n_init * (chi_d_max_new - chi_d_min) for i in range(0, points_n_init)]
    vals_uni = []
    for chi_d in chi_d_array_uni:
        if fabs(normaliser(tan(chi_d) - func_chi_o_gamma(chi_d), max_num)) > func_cut_off:
            continue
        vals_uni.append((chi_d, normaliser(tan(chi_d) - func_chi_o_gamma(chi_d), max_num)))
    chi_d_min_new = vals_uni[0][0]
    chi_d_max_new = vals_uni[-1][0]
    y_span = vals_uni[-1][1] - vals_uni[0][1]
    x_span = vals_uni[-1][0] - vals_uni[0][0]
    dy = y_span / points_chi_d
    dx_o_2 = x_span / (2 * points_chi_d)
    chi_d_array = [chi_d_min_new]
    gamma_over_chi = [normaliser((func_chi_o_gamma(chi_d_min_new)), max_num)]
    l = 0
    r = len(gamma_over_chi) - 1
    tan_chi_d_array = [normaliser(tan(chi_d_min_new), max_num)]
    tan_m_g_o_c = [tan_chi_d_array[0] - gamma_over_chi[0]]
    for i in range(points_chi_d):
        deriv = normaliser(deriv_odd(a, chi_d_array[-1] + dx_o_2), max_num)
        dx = min(dy / deriv, x_span / points_chi_d)
        if chi_d_array[-1] + dx + dx_o_2 >= chi_d_max_new:
            break
        chi_d_array.append(chi_d_array[-1] + dx)
        gamma_over_chi.append(normaliser(func_chi_o_gamma(chi_d_array[-1]), max_num))
        # tan_chi_d_array.append(normaliser(tan(chi_d_array[-1]), max_num))
        tan_m_g_o_c.append(normaliser(tan(chi_d_array[-1]) - gamma_over_chi[-1], max_num))
    gamma_over_chi_m_tan = tan_m_g_o_c
    l = 0
    r = len(gamma_over_chi) - 1
    if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] > 0:
        raise ValueError('There is no solution in this region')
    while gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[r] <= 0 and r - l > 1:
        m = int(l + (r - l) / 2)
        if gamma_over_chi_m_tan[l] * gamma_over_chi_m_tan[m] < 0:
            r = m
        elif gamma_over_chi_m_tan[r] * gamma_over_chi_m_tan[m] < 0:
            l = m
        else:
            break
    return chi_d_array[m]


def get_beta_by_chi_d(chi_d, lmbd, W, n1, n3):
    d = W / 2
    return sqrt(n1 ** 2 * (2 * pi / lmbd) ** 2 - (chi_d) ** 2 / d ** 2)


def get_gamma_by_beta(beta, lmbd, W, n1, n3):
    d = W / 2
    return sqrt(beta ** 2 - n3 ** 2 * (2 * pi) ** 2 / lmbd ** 2)


def get_coupling_coeff_even(del_R, chi, gamma, beta, lmbd, W, n1, n3):
    d = W / 2
    alpha = del_R - d
    return fabs((4 * (n1 ** 2 - n3 ** 2) * cos(chi * d) * exp(-gamma * alpha) * (pi ** 2)
            * (gamma * cos(d * chi) * sinh(gamma * d) + chi * sin(chi * d) * cosh(gamma * d))) / \
           (beta * (lmbd * n1) ** 2 * (gamma ** 2 + chi ** 2) * (d + sin(2 * chi * d) / (2 * chi)
                                                                 + cos(chi * d) ** 2 / gamma)))


def get_coupling_coeff_odd(del_R, chi, gamma, beta, lmbd, W, n1, n3):
    d = W / 2
    alpha = del_R - d
    return fabs((4 * (n1 ** 2 - n3 ** 2) * sin(chi * d) * exp(-gamma * alpha) * (pi ** 2) * (chi - chi * cos (chi * d)
                                * cosh(gamma * d) + gamma * sin(chi * d) * sinh(gamma * d)) ) / \
                (beta * (lmbd * n1) ** 2 * (gamma ** 2 + chi ** 2) * (d - sin(2 * chi * d) / (2 * chi)
                                                                      + sin(chi * d) ** 2 / gamma)))
    # return fabs((4 * (n1 ** 2 - n3 ** 2) * sin(chi * d) * exp(-gamma * alpha) * (pi ** 2) * cosh(chi * d)
    #         * (chi * cos(d * chi) - gamma * sin(chi * d))) / \
    #        (beta * (lmbd * n1) ** 2 * (gamma ** 2 + chi ** 2) * (d - sin(2 * chi * d) / (2 * chi)
    #                                                              + sin(chi * d) ** 2 / gamma)))


def print_result_for_given_L(L, c1, c2):
    print(f'sin(L*|c_1|) = {sin(L * fabs(c1))}')
    print(f'sin(L*|c_2|) = {sin(L * fabs(c2))}')
    print(f'cos(L*|c_1|) = {cos(L * fabs(c1))}')
    print(f'cos(L*|c_2|) = {cos(L * fabs(c2))}')


def get_L_for_c_2_over_c1(digital_precision, c1, c2):
    # find minimal L so that cos(L * c_even) = 1 and sin(L * c_even) = 0
    # and cos(L * c_odd) = 0 and sin(L * c_odd) = 1
    # eps > sin(L * c_odd)
    # eps must be not bigger than 0.01 (so that 1/(4 * eps) is integer)
    # if digital_precision < 2:
    #     raise ValueError('Too low precision')
    eps = 10 ** (-digital_precision)
    c_odd_over_c_even = fabs(c2 / c1)
    alpha_cut = round(c_odd_over_c_even, digital_precision)
    mu_even = 1 / (4 * eps)
    mu_odd = ceil((alpha_cut * 4 * mu_even - 1) / 4)
    L = fabs((4 * mu_odd + 1) * pi / 2 / c2)
    print(f'sin(L*|c_1|) = {sin(L * fabs(c1))}')
    print(f'sin(L*|c_2|) = {sin(L * fabs(c2))}')
    print(f'cos(L*|c_1|) = {cos(L * fabs(c1))}')
    print(f'cos(L*|c_2|) = {cos(L * fabs(c2))}')
    return L


def compute_new_R_old(basic_c1_o_c0, basic_R, gamma0, gamma1):
    # find mu_0 and mu_1 so c1_o_c0 = (4*mu_1 + 1) / (4 * mu_0)
    mu_1 = ceil((4 * basic_c1_o_c0 - 1) / 4)
    mu_0 = 1
    c1_o_c0 = (4 * mu_1 + 1) / (4 * mu_0)
    return 1 / (gamma0 - gamma1) * (log(c1_o_c0 / basic_c1_o_c0, e)) + basic_R


def compute_new_R(basic_c1_o_c0, basic_R, gamma0, gamma1):
    # find mu_0 and mu_1 so c1_o_c0 = (4*mu_1 + 1) / (4 * mu_0)
    if basic_c1_o_c0 >= 1:
        mu_1 = ceil((4 * basic_c1_o_c0 - 1) / 4)
        mu_0 = 1
        c1_o_c0 = (4 * mu_1 + 1) / (4 * mu_0)
        return 1 / (gamma0 - gamma1) * (log(c1_o_c0 / basic_c1_o_c0, e)) + basic_R, (mu_0, mu_1)
    else:
        mu_0 = floor((1 / 4) * (1 / basic_c1_o_c0 * (4 + 1)))
        mu_1 = 1
        c1_o_c0 = (4 * mu_1 + 1) / (4 * mu_0)
        return 1 / (gamma0 - gamma1) * (log(c1_o_c0 / basic_c1_o_c0, e)) + basic_R, (mu_0, mu_1)


def get_mu_pair_to_minimize_L_c1_o_c0(c1_o_c0, gamma1_o_gamma0, mu_0_max=100):
    mu_0_array = [i for i in range(1, mu_0_max + 1)]
    res = (mu_0_array[0], ceil(mu_0_array[0] * c1_o_c0 - 1/4))

    def get_L_var(mu_pair, gamma1_o_gamma0):
        """ not L itself, needs a multiplier yet """
        return (4 * mu_pair[1] + 1) / (mu_pair[0] ** gamma1_o_gamma0)

    min_L = get_L_var(res, gamma1_o_gamma0)
    for mu_0 in mu_0_array:
        mu_1 = ceil(mu_0 * c1_o_c0 - 1 / 4)
        L = get_L_var((mu_0, mu_1), gamma1_o_gamma0)
        if L < min_L:
            res = (mu_0, mu_1)
            min_L = L
    return res


def get_mu_pair_to_minimize_L_c0_o_c1(c0_o_c1, gamma0, gamma1, mu_1_max=100):
    mu_1_array = [i for i in range(0, mu_1_max + 1)]
    res = (ceil((4 * mu_1_array[0] + 1) * c0_o_c1 / 4), mu_1_array[0])

    def get_L_var(mu_pair, gamma0, gamma1):
        """ not L itself, needs a multiplier yet """
        return (mu_pair[0]) ** ((2 * gamma0 - gamma1) / (gamma0 - gamma1)) * (4 * mu_pair[1] + 1) ** (-gamma0 / (gamma0 - gamma1))

    min_L = get_L_var(res, gamma0, gamma1)
    for mu_1 in mu_1_array:
        mu_0 = ceil((4 * mu_1 + 1) * c0_o_c1 / 4)
        # mu_0 = 100
        L = get_L_var((mu_0, mu_1), gamma0, gamma1)
        if L < min_L:
            res = (mu_0, mu_1)
            min_L = L
    return res


def compute_new_R_adv(basic_c1_o_c0, basic_R, gamma0, gamma1):
    # find mu_0 and mu_1 so c1_o_c0 = (4*mu_1 + 1) / (4 * mu_0)
    if basic_c1_o_c0 >= 1:
        mu_0, mu_1 = get_mu_pair_to_minimize_L_c1_o_c0(basic_c1_o_c0, gamma1 / gamma0)
        c1_o_c0 = (4 * mu_1 + 1) / (4 * mu_0)
        return 1 / (gamma0 - gamma1) * (log(c1_o_c0 / basic_c1_o_c0, e)) + basic_R, (mu_0, mu_1)
    else:
        mu_0, mu_1 = get_mu_pair_to_minimize_L_c1_o_c0(basic_c1_o_c0, gamma1 / gamma0)
        c1_o_c0 = (4 * mu_1 + 1) / (4 * mu_0)
        # return -1 / (gamma0 - gamma1) * (log(basic_c1_o_c0 / c1_o_c0, e)) + basic_R, (mu_0, mu_1)
        return 1 / (gamma0 - gamma1) * (log(c1_o_c0 / basic_c1_o_c0, e)) + basic_R, (mu_0, mu_1)


def print_system_transformation(c_0, c_1, L):
    print(f'cos(L*|c_0|) = {cos(L * fabs(c_0))} : 1 expected')
    print(f'sin(L*|c_0|) = {sin(L * fabs(c_0))} : 0 expected')
    print(f'cos(L*|c_1|) = {cos(L * fabs(c_1))} : 0 expected')
    print(f'sin(L*|c_1|) = {sin(L * fabs(c_1))} : 1 expected')


def compute_L_by_correct_c0(c0, mu_pair):
    return (2 * pi * mu_pair[0]) / fabs(c0)


def compute_L_by_correct_c1(c1, mu_pair):
    return (2 * pi * mu_pair[1] + 1/2 * pi) / fabs(c1)


def get_L_and_coeffs_by_sys_params(lmbd, W, n1, n3, basic_R, points_chi_d, verbose=False):
    eps = 0.001
    chi_d_max = 20
    R = basic_R
    # chi_d = [i / points_chi_d * chi_d_max for i in range(0, points_chi_d)]
    chi_d_1 = get_even_mode_beta_adv(points_chi_d, eps, pi / 2 - eps, lmbd, W, n1, n3)
    chi_d_2 = get_odd_mode_beta_adv(points_chi_d, pi / 2 + eps, 3 * pi / 2 - eps, lmbd, W, n1, n3)
    beta_1 = get_beta_by_chi_d(chi_d_1, lmbd, W, n1, n3)
    beta_2 = get_beta_by_chi_d(chi_d_2, lmbd, W, n1, n3)
    gamma_1 = get_gamma_by_beta(beta_1, lmbd, W, n1, n3)
    gamma_2 = get_gamma_by_beta(beta_2, lmbd, W, n1, n3)
    c_1 = get_coupling_coeff_even(R, 2 * chi_d_1 / W, gamma_1, beta_1, lmbd, W, n1, n3)
    c_2 = get_coupling_coeff_odd(R, 2 * chi_d_2 / W, gamma_2, beta_2, lmbd, W, n1, n3)
    new_R, mu_pair = compute_new_R_adv(c_2 / c_1, R, gamma_1, gamma_2)
    if verbose:
        print(f'chi_d_1 = {chi_d_1} mkm')
        print(f'chi_d_2 = {chi_d_2} mkm')
        print(f'beta_1 = {beta_1} mkm')
        print(f'beta_2 = {beta_2} mkm')
        print(f'gamma_1 = {gamma_1} mkm')
        print(f'gamma_2 = {gamma_2} mkm')
    c_1_new = get_coupling_coeff_even(new_R, 2 * chi_d_1 / W, gamma_1, beta_1, lmbd, W, n1, n3)
    c_2_new = get_coupling_coeff_odd(new_R, 2 * chi_d_2 / W, gamma_2, beta_2, lmbd, W, n1, n3)
    if c_2 > c_1:
        L = compute_L_by_correct_c0(c_1_new, mu_pair)
    else:
        L = compute_L_by_correct_c1(c_2_new, mu_pair)
    # if 1.92 < W/2 < 1.94:
    #     print('here')
    return L, c_1_new, c_2_new, chi_d_1, chi_d_2, c_1, c_2, new_R, gamma_1, gamma_2, beta_1, beta_2
