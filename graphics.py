from math import sqrt, tan, pi, fabs, exp, cos
import matplotlib.pyplot as plt
import computation


def max_fun_by_abs(func, max_abs):
    res = []
    for i in func:
        if i > 0:
            res.append(min(i, max_abs))
        else:
            res.append(max(i, -max_abs))
    return res


def plot_even(points_n, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    chi_d = [chi_d_min + i / points_n * (chi_d_max - chi_d_min) for i in range(0, points_n)]
    # gamma_over_chi = [sqrt((n1 ** 2 - n2 ** 2) * (2 * pi / lmbd) ** 2 - i ** 2 / d ** 2) / (i / d) for i in chi_d]
    gamma_over_chi = []
    for i in chi_d:
        try:
            gamma_over_chi.append(sqrt((n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2 / i ** 2 - 1))
        except ValueError:
            break
    tan_chi_d = [tan(i) for i in chi_d]
    tan_chi_d_normed = max_fun_by_abs(tan_chi_d, 10)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(chi_d[:len(gamma_over_chi)], max_fun_by_abs(gamma_over_chi, 10), 'k*', markersize=1)
    ax.plot(chi_d, tan_chi_d_normed, 'r*', markersize=1)
    ax.set_title('Even Modes solution')
    ax.grid()
    plt.show()


def plot_even_adv(points_n, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    eps = 0.0001
    max_num = 50
    func_cut_off = 3
    points_n_init = 500
    a = (n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2
    func_gamma_o_chi = lambda chi_d: sqrt(a / chi_d ** 2 - 1)
    deriv_even = lambda a_inner, chi_d: a_inner / (sqrt(a_inner / chi_d ** 2 - 1) * chi_d ** 3) + 1 / cos(chi_d) ** 2
    chi_d_max_new = min(chi_d_max, sqrt(n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) * d) - eps
    chi_d_array_uni = [chi_d_min + i / points_n_init * (chi_d_max_new - chi_d_min) for i in range(0, points_n_init)]
    vals_uni = []
    for chi_d in chi_d_array_uni:
        if fabs(computation.normaliser(tan(chi_d) - func_gamma_o_chi(chi_d), max_num)) > func_cut_off:
            continue
        vals_uni.append((chi_d, computation.normaliser(tan(chi_d) - func_gamma_o_chi(chi_d), max_num)))
    chi_d_min_new = vals_uni[0][0]
    chi_d_max_new = vals_uni[-1][0]
    y_span = vals_uni[-1][1] - vals_uni[0][1]
    x_span = vals_uni[-1][0] - vals_uni[0][0]
    dy = y_span / points_n
    dx_o_2 = x_span / (2 * points_n)
    chi_d_array = [chi_d_min_new]
    tan_chi_d_array = [computation.normaliser(tan(chi_d_min_new), max_num)]
    gamma_over_chi = [computation.normaliser((func_gamma_o_chi(chi_d_min_new)), max_num)]
    tan_m_g_o_c = [tan_chi_d_array[0] - gamma_over_chi[0]]
    deriv_array = [computation.normaliser(deriv_even(a, chi_d_array[-1] + dx_o_2), max_num)]
    for i in range(points_n):
        deriv = computation.normaliser(deriv_even(a, chi_d_array[-1] + dx_o_2), max_num)
        dx = min(dy / deriv, x_span / points_n)
        if chi_d_array[-1] + dx + dx_o_2 >= chi_d_max_new:
            break
        chi_d_array.append(chi_d_array[-1] + dx)
        gamma_over_chi.append(computation.normaliser(func_gamma_o_chi(chi_d_array[-1]), max_num))
        tan_chi_d_array.append(computation.normaliser(tan(chi_d_array[-1]), max_num))
        tan_m_g_o_c.append(computation.normaliser(tan(chi_d_array[-1]) - gamma_over_chi[-1], max_num))
        deriv_array.append(computation.normaliser(deriv, max_num))
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(chi_d_array, gamma_over_chi, 'k*', markersize=1)
    ax.plot(chi_d_array, tan_chi_d_array, 'r*', markersize=1)
    ax.plot(chi_d_array, tan_m_g_o_c, 'b*', markersize=1)
    # ax.plot(chi_d_array, deriv_array, 'y*', markersize=1)
    ax.set_title('Even Modes solution near 0')
    ax.grid()
    points_outside_left, points_outside_right = points_n, points_n
    chi_d_max_left = chi_d_array[0]
    chi_d_min_right = chi_d_array[-1]
    chi_d_max_new = min(chi_d_max, sqrt(n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) * d) - eps
    chi_d_array_left = [chi_d_min + (chi_d_max_left - chi_d_min) * i / points_outside_left
                       for i in range(points_outside_left)]
    chi_d_array_right = [chi_d_min_right + (chi_d_max_new - chi_d_min_right) * i / points_outside_right
                        for i in range(points_outside_right)]
    chi_d_array_ext = chi_d_array_left + chi_d_array + chi_d_array_right
    plot_max = 100
    gamma_over_chi_ext = [computation.normaliser(func_gamma_o_chi(chi_d), plot_max) for chi_d in chi_d_array_left] \
                         + gamma_over_chi \
                         + [computation.normaliser(func_gamma_o_chi(chi_d), plot_max) for chi_d in chi_d_array_right]
    tan_chi_d_array_ext = [computation.normaliser(tan(chi_d), plot_max) for chi_d in chi_d_array_left] + tan_chi_d_array \
                         + [computation.normaliser(tan(chi_d), plot_max) for chi_d in chi_d_array_right]
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(1, 1, 1)
    ax1.plot(chi_d_array_ext, gamma_over_chi_ext, 'k*', markersize=1)
    ax1.plot(chi_d_array_ext, tan_chi_d_array_ext, 'r*', markersize=1)
    ax1.set_title('Even Modes solution global')
    ax1.grid()
    plt.show()


def plot_odd(points_n, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    chi_d = [chi_d_min + i / points_n * (chi_d_max - chi_d_min) for i in range(0, points_n)]
    # gamma_over_chi = [sqrt((n1 ** 2 - n2 ** 2) * (2 * pi / lmbd) ** 2 - i ** 2 / d ** 2) / (i / d) for i in chi_d]
    gamma_over_chi = []
    for i in chi_d:
        try:
            gamma_over_chi.append(-sqrt(1 / ((n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2 / i ** 2 - 1)))
        except ValueError:
            break
    tan_chi_d = [tan(i) for i in chi_d]
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    tan_chi_d_normed = max_fun_by_abs(tan_chi_d, 10)
    ax.plot(chi_d[:len(gamma_over_chi)], max_fun_by_abs(gamma_over_chi, 10), 'k*', markersize=1)
    ax.plot(chi_d, tan_chi_d_normed, 'r*', markersize=1)
    ax.set_title('Odd Modes solution')
    ax.grid()
    plt.show()


def plot_odd_adv(points_n, chi_d_min, chi_d_max, lmbd, W, n1, n3):
    d = W / 2
    eps = 0.00000001
    max_num = 500
    func_cut_off = 10
    points_n_init = max(500, points_n)
    a = (n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) ** 2 * d ** 2
    func_chi_o_gamma = lambda chi_d: -sqrt(1 / (a / chi_d ** 2 - 1))
    deriv_odd = lambda a_inner, chi_d: a_inner / (a_inner - chi_d ** 2) + 1 / cos(chi_d) ** 2
    chi_d_max_new = min(chi_d_max, sqrt(n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) * d) - eps
    chi_d_array_uni = [chi_d_min + i / points_n_init * (chi_d_max_new - chi_d_min) for i in range(0, points_n_init)]
    vals_uni = []
    for chi_d in chi_d_array_uni:
        if fabs(computation.normaliser(tan(chi_d) - func_chi_o_gamma(chi_d), max_num)) > func_cut_off:
            continue
        vals_uni.append((chi_d, computation.normaliser(tan(chi_d) - func_chi_o_gamma(chi_d), max_num)))
    chi_d_min_new = vals_uni[0][0]
    chi_d_max_new = vals_uni[-1][0]
    y_span = vals_uni[-1][1] - vals_uni[0][1]
    x_span = vals_uni[-1][0] - vals_uni[0][0]
    dy = y_span / points_n
    dx_o_2 = x_span / (2 * points_n)
    chi_d_array = [chi_d_min_new]
    tan_chi_d_array = [computation.normaliser(tan(chi_d_min_new), max_num)]
    gamma_over_chi = [computation.normaliser((func_chi_o_gamma(chi_d_min_new)), max_num)]
    tan_m_g_o_c = [tan_chi_d_array[0] - gamma_over_chi[0]]
    deriv_array = [computation.normaliser(deriv_odd(a, chi_d_array[-1] + dx_o_2), max_num)]
    for i in range(points_n):
        deriv = computation.normaliser(deriv_odd(a, chi_d_array[-1] + dx_o_2), max_num)
        dx = min(dy / deriv, x_span / points_n)
        if chi_d_array[-1] + dx + dx_o_2 >= chi_d_max_new:
            break
        chi_d_array.append(chi_d_array[-1] + dx)
        gamma_over_chi.append(computation.normaliser(func_chi_o_gamma(chi_d_array[-1]), max_num))
        tan_chi_d_array.append(computation.normaliser(tan(chi_d_array[-1]), max_num))
        tan_m_g_o_c.append(computation.normaliser(tan(chi_d_array[-1]) - gamma_over_chi[-1], max_num))
        deriv_array.append(computation.normaliser(deriv, max_num))
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(chi_d_array, gamma_over_chi, 'k*', markersize=1)
    ax.plot(chi_d_array, tan_chi_d_array, 'r*', markersize=1)
    ax.plot(chi_d_array, tan_m_g_o_c, 'b*', markersize=1)
    # ax.plot(chi_d_array, deriv_array, 'y*', markersize=1)
    ax.set_title('Odd Modes solution near 0')
    ax.grid()
    points_outside_left, points_outside_right = points_n, points_n
    chi_d_max_left = chi_d_array[0]
    chi_d_min_right = chi_d_array[-1]
    chi_d_max_new = min(chi_d_max, sqrt(n1 ** 2 - n3 ** 2) * (2 * pi / lmbd) * d) - eps
    chi_d_array_left = [chi_d_min + (chi_d_max_left - chi_d_min) * i / points_outside_left
                       for i in range(points_outside_left)]
    chi_d_array_right = [chi_d_min_right + (chi_d_max_new - chi_d_min_right) * i / points_outside_right
                        for i in range(points_outside_right)]
    chi_d_array_ext = chi_d_array_left + chi_d_array + chi_d_array_right
    plot_max = 100
    gamma_over_chi_ext = [computation.normaliser(func_chi_o_gamma(chi_d), plot_max) for chi_d in chi_d_array_left] \
                         + gamma_over_chi \
                         + [computation.normaliser(func_chi_o_gamma(chi_d), plot_max) for chi_d in chi_d_array_right]
    tan_chi_d_array_ext = [computation.normaliser(tan(chi_d), plot_max) for chi_d in chi_d_array_left] + tan_chi_d_array \
                         + [computation.normaliser(tan(chi_d), plot_max) for chi_d in chi_d_array_right]
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(1, 1, 1)
    ax1.plot(chi_d_array_ext, gamma_over_chi_ext, 'k*', markersize=1)
    ax1.plot(chi_d_array_ext, tan_chi_d_array_ext, 'r*', markersize=1)
    ax1.set_title('Odd Modes solution global')
    ax1.grid()
    plt.show()



def plot_c_ratio_of_del_R(n_points_del_R, lmbd, n1, n3, W):
    fig = plt.figure()
    ax = fig.add_subplot(2, 1, 1)
    ax.set_title('c_2/c_1(R)')
    del_R_min = 1  # mkm
    del_R_max = 10  # mkm
    del_R_array_lin = [del_R_min + i * (del_R_max - del_R_min) / n_points_del_R for i in range(n_points_del_R)]
    # del_R_array_exp = [del_R_min * exp(1 - i / n_points_del_R) for i in range(n_points_del_R)]
    c2_over_c1_array = []
    points_n = 1000
    for del_R in del_R_array_lin:
        chi_d_1 = computation.get_even_mode_beta(points_n, 0.01, pi / 2 - 0.01, lmbd, W, n1, n3)
        chi_d_2 = computation.get_odd_mode_beta(points_n, pi / 2 + 0.01, 3 * pi / 2 - 0.01, lmbd, W, n1, n3)
        beta_1 = computation.get_beta_by_chi_d(chi_d_1, lmbd, W, n1, n3)
        beta_2 = computation.get_beta_by_chi_d(chi_d_2, lmbd, W, n1, n3)
        gamma_1 = computation.get_gamma_by_beta(beta_1, lmbd, W, n1, n3)
        gamma_2 = computation.get_gamma_by_beta(beta_2, lmbd, W, n1, n3)
        c_1 = computation.get_coupling_coeff_even(del_R, 2 * chi_d_1 / W, gamma_1, beta_1, lmbd, W, n1, n3)
        c_2 = computation.get_coupling_coeff_odd(del_R, 2 * chi_d_2 / W, gamma_2, beta_2, lmbd, W, n1, n3)
        c2_over_c1_array.append(fabs(c_2 / c_1))
    ax.plot(del_R_array_lin, c2_over_c1_array, '*k', markersize=1)
    ax.set_yscale('log')
    ax.grid()
    plt.show()


def plot_c1_of_del_R(n_points_del_R, lmbd, n1, n3, W):
    fig = plt.figure()
    ax = fig.add_subplot(2, 1, 1)
    ax.set_title('c_1(R)')
    del_R_min = 1  # mkm
    del_R_max = 10  # mkm
    del_R_array_lin = [del_R_min + i * (del_R_max - del_R_min) / n_points_del_R for i in range(n_points_del_R)]
    # del_R_array_exp = [del_R_min * exp(1 - i / n_points_del_R) for i in range(n_points_del_R)]
    c1_array = []
    points_n = 1000
    for del_R in del_R_array_lin:
        chi_d_1 = computation.get_even_mode_beta(points_n, 0.01, pi / 2 - 0.01, lmbd, W, n1, n3)
        chi_d_2 = computation.get_odd_mode_beta(points_n, pi / 2 + 0.01, 3 * pi / 2 - 0.01, lmbd, W, n1, n3)
        beta_1 = computation.get_beta_by_chi_d(chi_d_1, lmbd, W, n1, n3)
        beta_2 = computation.get_beta_by_chi_d(chi_d_2, lmbd, W, n1, n3)
        gamma_1 = computation.get_gamma_by_beta(beta_1, lmbd, W, n1, n3)
        gamma_2 = computation.get_gamma_by_beta(beta_2, lmbd, W, n1, n3)
        c_1 = computation.get_coupling_coeff_even(del_R, 2 * chi_d_1 / W, gamma_1, beta_1, lmbd, W, n1, n3)
        c1_array.append(fabs(c_1))
    ax.plot(del_R_array_lin, c1_array, '*k', markersize=1)
    ax.set_yscale('log')
    ax.grid()
    plt.show()


def plot_c2_of_del_R(n_points_del_R, lmbd, n1, n3, W):
    fig = plt.figure()
    ax = fig.add_subplot(2, 1, 1)
    ax.set_title('c_2(R)')
    del_R_min = 1  # mkm
    del_R_max = 10  # mkm
    del_R_array_lin = [del_R_min + i * (del_R_max - del_R_min) / n_points_del_R for i in range(n_points_del_R)]
    # del_R_array_exp = [del_R_min * exp(1 - i / n_points_del_R) for i in range(n_points_del_R)]
    c2_array = []
    points_n = 1000
    for del_R in del_R_array_lin:
        chi_d_1 = computation.get_even_mode_beta_adv(points_n, 0.01, pi / 2 - 0.01, lmbd, W, n1, n3)
        chi_d_2 = computation.get_odd_mode_beta_adv(points_n, pi / 2 + 0.01, 3 * pi / 2 - 0.01, lmbd, W, n1, n3)
        beta_1 = computation.get_beta_by_chi_d(chi_d_1, lmbd, W, n1, n3)
        beta_2 = computation.get_beta_by_chi_d(chi_d_2, lmbd, W, n1, n3)
        gamma_1 = computation.get_gamma_by_beta(beta_1, lmbd, W, n1, n3)
        gamma_2 = computation.get_gamma_by_beta(beta_2, lmbd, W, n1, n3)
        c_2 = computation.get_coupling_coeff_odd(del_R, 2 * chi_d_1 / W, gamma_1, beta_1, lmbd, W, n1, n3)
        c2_array.append(fabs(c_2))
    ax.plot(del_R_array_lin, c2_array, '*k', markersize=1)
    ax.set_yscale('log')
    ax.grid()
    plt.show()


def plot_c_ratio_of_d(n_points_d, lmbd, n1, n3, R):
    fig = plt.figure()
    fig1 = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title(f'$c^1/c^0(d)$')
    ax1 = fig1.add_subplot(1, 1, 1)
    ax1.set_title(f'$c_1(d)$')
    d_min = lmbd / (4 * sqrt(n1 ** 2 - n3 ** 2)) + 0.02
    d_max = d_min * 2 - 0.02
    d_array = [d_min + i * (d_max - d_min) / n_points_d for i in range(n_points_d)]
    c2_over_c1_array = []
    c1_array = []
    points_n = 3000
    eps = 0.001
    for d in d_array:
        W = 2 * d
        chi_d_1 = computation.get_even_mode_beta_adv(points_n, eps, pi / 2 - eps, lmbd, W, n1, n3)
        chi_d_2 = computation.get_odd_mode_beta_adv(points_n, pi / 2 + eps, 3 * pi / 2 - eps, lmbd, W, n1, n3)
        beta_1 = computation.get_beta_by_chi_d(chi_d_1, lmbd, W, n1, n3)
        beta_2 = computation.get_beta_by_chi_d(chi_d_2, lmbd, W, n1, n3)
        gamma_1 = computation.get_gamma_by_beta(beta_1, lmbd, W, n1, n3)
        gamma_2 = computation.get_gamma_by_beta(beta_2, lmbd, W, n1, n3)
        c_1 = computation.get_coupling_coeff_even(R, 2 * chi_d_1 / W, gamma_1, beta_1, lmbd, W, n1, n3)
        c_2 = computation.get_coupling_coeff_odd(R, 2 * chi_d_2 / W, gamma_2, beta_2, lmbd, W, n1, n3)
        c2_over_c1_array.append(c_2 / c_1)
        c1_array.append(c_1)
    ax.plot(d_array, c2_over_c1_array, 'k', markersize=1)
    ax.grid()
    ax.set_yscale('log')
    ax1.plot(d_array, c1_array, 'k', markersize=1)
    ax1.grid()
    ax1.set_yscale('log')
    plt.show()


def plot_L_of_d(n_points_d, lmbd, n1, n3):
    fig = plt.figure()
    fig1 = plt.figure()
    fig2 = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax1 = fig1.add_subplot(1, 1, 1)
    ax2 = fig2.add_subplot(1, 1, 1)
    ax.set_title('L(d)')
    ax1.set_title('c1(d)')
    ax2.set_title('c2(d)')
    eps = 0.1
    d_min = lmbd / (4 * sqrt(n1 ** 2 - n3 ** 2)) + eps
    d_max = lmbd / (2 * sqrt(n1 ** 2 - n3 ** 2)) - eps
    d_array = [d_min + (d_max - d_min) * i / n_points_d for i in range(n_points_d)]
    L_array = []
    c_1_array = []
    c_2_array = []
    points_n = 1000
    R = 2
    for d in d_array:
        params = computation.get_L_and_coeffs_by_sys_params(lmbd, 2 * d, n1, n3, R, points_n)
        L_array.append(params[0])
        c_1_array.append(params[1])
        c_2_array.append(params[2])
    ax.set_xlabel('d, mkm')
    ax.set_ylabel('L, mkm')
    ax.plot(d_array, L_array, 'k', markersize=1)
    ax1.plot(c_1_array, L_array, 'k', markersize=1)
    ax2.plot(c_2_array, L_array, 'k', markersize=1)
    ax.grid()
    ax1.grid()
    ax2.grid()
    ax.set_yscale('log')
    ax1.set_yscale('log')
    ax2.set_yscale('log')
    plt.show()


def plot_of_d(n_points_d, lmbd, n1, n3, R=1):
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    n_plots = 8
    figs = [plt.figure() for i in range(n_plots)]
    axes = [fig.add_subplot(1, 1, 1) for fig in figs]
    titles = ['L(d)', r'$c^0(d)$', r'$c^1(d)$', r'$\chi^0d(d)$', r'$\chi^1d(d)$', r'$c^0(d)$', r'$c^1(d)$',
              r'$\Delta R(d)$']
    [ax.set_title(titles[i]) for i, ax in enumerate(axes)]
    eps = 0.05
    d_min = lmbd / (4 * sqrt(n1 ** 2 - n3 ** 2)) + eps
    d_max = lmbd / (2 * sqrt(n1 ** 2 - n3 ** 2)) - eps
    d_array = [d_min + (d_max - d_min) * i / n_points_d for i in range(n_points_d)]
    values_arrays = []
    points_n = 3000
    for plt_ind in range(n_plots):
        values_arrays.append([])
    for d in d_array:
        params = computation.get_L_and_coeffs_by_sys_params(lmbd, 2 * d, n1, n3, 2 * d, points_n)
        params_mod = params[0:7] + (params[7] - 2 * d,)
        for plt_ind in range(n_plots):
            values_arrays[plt_ind].append(params_mod[plt_ind])
    y_labels = [r'L, $\mu m$', r'$c^0, mkm^{-1}$', r'$c^1, mkm^{-1}$', r'$\chi^0d, mkm^{-1}$', r'$\chi^1d, mkm^{-1}$',
                r'$c^0, mkm^{-1}$', r'$c^1, mkm^{-1}$', r'$\Delta R, \mu_m$']
    x_labels = ['d, $\mu m$', 'd, $\mu m$', 'd, $\mu m$', 'd, $\mu m$', 'd, $\mu m$',
                'd, $\mu m$', 'd, $\mu m$', 'd, $\mu m$']
    for i, ax in enumerate(axes):
        ax.set_xlabel(x_labels[i])
        ax.set_ylabel(y_labels[i])
        ax.plot(d_array, values_arrays[i], ".k", markersize=3)
        if i != 7:
            ax.set_yscale('log')
        ax.grid()
    plt.show()
